package sample;

import com.jfoenix.controls.JFXToggleButton;
import javafx.scene.control.ToggleGroup;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 * Расчет чаевых
 *
 * @author Andreeva V.A.
 */
public class Controller {
    private double DOLLAR_COURSE = 72.66;
    private double EURO_COURSE = 79.31;

    @FXML
    private ComboBox<?> currency;

    @FXML
    private TextField amountField;

    @FXML
    private RadioButton noPrecentRB;

    @FXML
    private RadioButton fivePrecentRB;

    @FXML
    private RadioButton tenPrecentRB;

    @FXML
    private TextField amountTipField;

    @FXML
    private JFXToggleButton inRubTB;

    @FXML
    private JFXToggleButton formatTB;

    @FXML
    private TextField allAmountField;

    @FXML
    private TextField allAmountInRub;

    @FXML
    private TextField amountTipInRub;

    /**
     * Расчет чаевых
     */
    @FXML
    void calculate(ActionEvent event) {
        if (returnAmountBill() != null) {
            double amount = returnAmountBill();
            if (amount > 0) {
                double percent = getPercent();
                double fullAmount = amount;
                double amountOfTip = 0;
                if (percent == 0.05 || percent == 0.1) {
                    fullAmount = amount + (amount * percent);
                    amountOfTip = amount * percent;
                }
                allAmountField.setText(String.valueOf(roundsNumbers(round(fullAmount))));
                amountTipField.setText(String.valueOf(roundsNumbers(round(amountOfTip))));

            } else {
                amountField.setText("Введите число больше нуля!");
            }
        }
    }

    /**
     * Возвращает процент чаевых, в зависимости от выбранной радиокнопки:
     * 1, если выбрана радиокнопка "не оставлять чаевые";
     * 0,05, если выбрана радиокнопка 5%;
     * 0,1, если выбрана радиокнопка 10%
     *
     * @return вещ. число - процент
     */
    private double getPercent() {
        ToggleGroup group = new ToggleGroup();
        fivePrecentRB.setToggleGroup(group);
        tenPrecentRB.setToggleGroup(group);
        noPrecentRB.setToggleGroup(group);
        RadioButton selectedButton = (RadioButton) group.getSelectedToggle();

        if (selectedButton == fivePrecentRB) {
            return 0.05;
        }
        if (selectedButton == tenPrecentRB) {
            return 0.1;
        }
        return Double.parseDouble(null);
    }

    /**
     * Метод перевода чаевых и общей суммы из ин. валюты в рубли
     */
    @FXML
    void transferInRubble(ActionEvent event) {
        if (inRubTB.isSelected()) {
            double percent = getPercent();
            double courseOfCurrency = returnChosenCourseOfCurrency();
            double amountOfBill = returnAmountBill();

            double rubbleAmount = (amountOfBill * courseOfCurrency);
            double tipsInRubble = 0;

            if (percent == 0.05 || percent == 0.1) {
                rubbleAmount = (amountOfBill * courseOfCurrency) + (amountOfBill * percent);
                tipsInRubble = amountOfBill * courseOfCurrency * percent;
            }
            allAmountInRub.setText(String.valueOf(roundsNumbers(round(rubbleAmount))));
            amountTipInRub.setText(String.valueOf(roundsNumbers(round(tipsInRubble))));
        }

    }

    /**
     * Возвращает курс выбранной валюты
     *
     * @return вещ. число
     */
    private double returnChosenCourseOfCurrency() {
        double courseOfCurrency = 1;
        if (currency.getValue().equals("USD")) {
            courseOfCurrency = DOLLAR_COURSE;
        }
        if (currency.getValue().equals("EUR")) {
            courseOfCurrency = EURO_COURSE;
        }
        return courseOfCurrency;
    }

    @FXML
    private boolean isSelected(JFXToggleButton toggleButton) {
        return toggleButton.isSelected();
    }

    /**
     * Осуществляет проверку на валидность введенные данные(сумму чека),
     * если все корректно - возвращает вещественное значение,
     * иначе сообщает об ошибке с последующей анимацией полей
     *
     * @return вещественное значение
     */
    private Double returnAmountBill() {
        try {
            return Double.parseDouble(String.valueOf(amountField.getText()).replace(",", "."));
        } catch (Exception e) {
            amountField.setText(" Введите число ");
        }
        return null;
    }

    /**
     * Возвращает вещественное число, округленное до 2-х знаков после запятой.
     *
     * @param number вещественное число
     * @return округленное вещественное число
     */
    private Double roundsNumbers(Double number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    private double round(Double numbers) {
        if (isSelected(formatTB)) {
            if (getPercent() == 3) {
                if (numbers % 1 != 0) {
                    numbers += 0.5;
                }
            }
            return new BigDecimal(numbers).setScale(0, RoundingMode.HALF_UP).doubleValue();
        }
        return numbers;
    }

    /**
     * Метод очистки полей при нажатии на кнопку "Очистить"
     */
    @FXML
    void clear(ActionEvent event) {
        amountField.clear();
        amountTipField.clear();
        amountTipInRub.clear();
        allAmountField.clear();
        allAmountInRub.clear();
    }
}

